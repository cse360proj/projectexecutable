import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;


import java.awt.*;
import java.awt.event.*;
import java.io.File;

public class InputScreen extends JPanel
{
	public JButton sumbitButton, helpButton, browseButton;
	public File[] files;
	
	private JTextArea output;
	private JLabel browseFile;
	private JFrame frame;
	public panelControl control;
	
	final JFileChooser fc = new JFileChooser();
	
	public InputScreen()
	{	
		helpButton = new JButton("Help");
		browseButton = new JButton("Browse");
		sumbitButton = new JButton("Submit");
		browseFile = new JLabel("File Name:     \n");
		frame = new JFrame("JOptionPane example");
		
		//Creating the window
		JPanel console = new JPanel();
		console.setLayout(new BoxLayout(console,BoxLayout.X_AXIS));
		console.setLayout(new GridLayout(1,1));
		this.add(console);
		
		//Dividing into 3 parts: top, middle, bottom
		JPanel p2 = new JPanel();
		p2.setLayout(new GridLayout (3,1));
		this.add(p2);
		
		//Top 
		JPanel top = new JPanel();
		top.setLayout(new BorderLayout());
		top.add(browseFile, "West");
		top.add(browseButton, "East");
		this.add(top);
		
		//Middle 
		JPanel middle = new JPanel();
		middle.setLayout(new BorderLayout());
		middle.add(Box.createRigidArea(new Dimension(50, 10)));
		middle.add(helpButton, "West");
		middle.add(sumbitButton, "East");
		this.add(middle);
		
		//Bottom
		JPanel bottom = new JPanel();
		bottom.setLayout(new BoxLayout(bottom, BoxLayout.X_AXIS));
		this.add(bottom);
		bottom.add(Box.createRigidArea(new Dimension(50, 10)));
		output = new JTextArea(7,35);
		output.setEditable(false);
		bottom.add(output);
		
		sumbitListener submit = new sumbitListener();
		helpListener help = new helpListener();
		browseListener browse = new browseListener();
		
		sumbitButton.addActionListener(submit);
		helpButton.addActionListener(help);
		browseButton.addActionListener(browse);
	
	}
	private class helpListener implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			
			JOptionPane.showMessageDialog(frame, 
					"In order to use this text file analyzer, you must first click \n"
					+ "browse and search your file directory for text files that you\n"
					+ "want to analyze. You can enter multiple files by clicking shift\n"
					+ "left click. Once you have chosen all the text files click the open\n"
					+ "button. The names should be printed out in the box below the button.\n"
					+ "If you wish to revise your inputs reclick on browse and pick your\n"
					+ "desired files. Once you are happy with your inputs click submit.\n"
					+ "You can now view your information by clicking on the tab named report.");
	
		}
	}
	private class sumbitListener implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			JOptionPane.showMessageDialog(frame, "Check the report tab for your information");
			control.printStats(files);
			control.printAvg(files);
		}
	}
	private class browseListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			//Handle browse button action
			if(e.getSource() == browseButton)
			{
				fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
				FileNameExtensionFilter filter = new FileNameExtensionFilter("Text File", "txt");
				fc.setAcceptAllFileFilterUsed(false);
				fc.setMultiSelectionEnabled(true);
				fc.setFileFilter(filter);
				int returnVal = fc.showOpenDialog(InputScreen.this);
				
				
				if (returnVal == JFileChooser.APPROVE_OPTION)
				{
					//Takes input files and stores them into an array of file type
					files = fc.getSelectedFiles();
					
					//load names into an array
					String[] fileName = FileInsert.printNames(files);
					
					//append all the names together, preceded by their position in the array
					String out = "";
					for(int i = 0; i < fileName.length; i++) 
					{
						out += Integer.toString(i) + ". " + fileName[i] + "\n";
					}
					
					//print names in output box
					output.setText(out);
					
					//
				}
				else
				{
					output.setText("No input taken.");
				}
			}
		}
		
	}
}
