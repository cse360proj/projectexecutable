import java.io.File;

public class panelControl {
	private InputScreen inputScreen;
	private ReportScreen reportScreen;
	private ReportScreen averageScreen;
	
	public panelControl(InputScreen input, ReportScreen report, ReportScreen average)
	{
		inputScreen = input;
		reportScreen = report;
		averageScreen = average;
	}
	
	public void printStats(File[] files)
	{
		reportScreen.text.setText(Analysis.analysisToString(files, false));
	}
	
	public void printAvg(File[] files)
	{
		averageScreen.text.setText(Analysis.analysisToString(files, true));
	}
}
