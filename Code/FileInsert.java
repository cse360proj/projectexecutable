import java.io.*;

public class FileInsert {

	public static String[] printNames(File[] files)
	{
		
		String[] names = new String[files.length];
		for(int i = 0; i < files.length; i++)
		{
			names[i] = files[i].getName();
			
		}
		return names;
	}
	
	public static File[] removeFile(File[] files, String fileName)
	{
		int index = files.length;
		for(int i = 0; i < files.length; i++)
		{
			if(files[i].getName().equals(fileName))
			{
				index--;
			}
		}
		
		File[] input = new File[index];
		int j = 0;
		for(int i = 0; i < files.length; i++)
		{
			if(!(files[i].getName().equals(fileName)))
			{
				input[j] = files[i];
				j++;
			}
		}
		return input;
	}
	
}
