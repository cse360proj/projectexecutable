import javax.swing.*;
import java.awt.*;

public class ReportScreen extends JPanel
{
	public JTextArea text;
	
	public ReportScreen()
	{
		JPanel bPanel = new JPanel(new BorderLayout());
		this.add(bPanel);
		text = new JTextArea(11, 30);
		text.setEditable(false);
		JScrollPane scroll = new JScrollPane(text);
		bPanel.add(scroll, BorderLayout.CENTER);
		
	}
	
}

