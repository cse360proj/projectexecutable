import javax.swing.*;

public class Main extends JFrame
{
	private int APPLICATION_WIDTH = 500, APPLICATION_HEIGHT = 270;
	
	private InputScreen inputScreen;
	private ReportScreen reportScreen;
	private ReportScreen averageScreen;
	private panelControl control;
	private JTabbedPane tPane;
	
	public Main()
	{
		inputScreen = new InputScreen();
		reportScreen = new ReportScreen();
		averageScreen = new ReportScreen();
		control = new panelControl(inputScreen, reportScreen, averageScreen);
		inputScreen.control = this.control;
		
		tPane = new JTabbedPane();
		tPane.addTab("Input", inputScreen);
		tPane.addTab("Report", reportScreen);
		tPane.addTab("Average", averageScreen);
		
		getContentPane().add(tPane);
		setSize(APPLICATION_WIDTH, APPLICATION_HEIGHT);
		setTitle("Text File Analyzer");
		setVisible(true);
	}
	
	public static void main(String args[])
	{
		new Main();
	}
}
