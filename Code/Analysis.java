import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Date;
import java.lang.Math;
public class Analysis {

	public static String analysisToString(File[] input, boolean avg)
	{
		Date date = new Date();
		String stats = "===============================================\n";
		ArrayList<String> list;
		if(avg == false)
		{
			for(int i = 0; i < input.length; i++)
			{
				list = fileToArrayList(input[i]);
				removePunctuation(list);
				stats += ("File " + i + "'s name is: " + input[i].getName() + "\n");
				stats += ("Number of lines: " + list.size() + "\n");
				stats += ("Number of blank lines: " + blankLineCount(list) + "\n");
				stats += ("Number of spaces: " + spacesCount(list) + "\n");
				stats += ("Number of words: " + wordCount(list) + "\n");
				stats += ("Average chars per line: " + avgChar(list) + "\n");
				stats += ("Average word length: " + avgWordLength(list) + "\n");
				stats += ("The most common word(s): " + mostCommonWord(list) + "\n");
				stats += ("This analysis was done on: " + date.toString() + "\n");
				stats += ("===============================================\n");
			}
		}
		else
		{
			list = averageFile(input);
			removePunctuation(list);
			stats += ("This is an average report of all the files\n");
			stats += ("Number of lines: " + list.size() + "\n");
			stats += ("Number of blank lines: " + blankLineCount(list) + "\n");
			stats += ("Number of spaces: " + spacesCount(list)  + "\n");
			stats += ("Number of words: " + wordCount(list) + "\n");
			stats += ("Average chars per line: " + avgChar(list) + "\n");
			stats += ("Average word length: " + avgWordLength(list) + "\n");
			stats += ("The most common word(s): " + mostCommonWord(list) + "\n");
			stats += ("This analysis was done on: " + date.toString() + "\n");
			stats += ("===============================================\n");
		}
		
		return stats;
		
	}
	
	public static ArrayList<String> fileToArrayList(File input)
	{
		ArrayList<String> list = new ArrayList<String>();
		try
		{
			FileReader fr = new FileReader(input);
			BufferedReader br = new BufferedReader(fr);
			StringBuffer strB = new StringBuffer();
			String line;
			while ((line = br.readLine()) != null)
			{
				strB.append(line);
				list.add(line);
			}
			fr.close();
		} 
		catch (IOException e)
		{
			System.out.print("IO error");
		}
		
		return list;
	}
	
	public static void fileToArrayList(File input, ArrayList<String> list)
	{
		try
		{
			FileReader fr = new FileReader(input);
			BufferedReader br = new BufferedReader(fr);
			StringBuffer strB = new StringBuffer();
			String line;
			while ((line = br.readLine()) != null)
			{
				strB.append(line);
				list.add(line);
			}
			fr.close();
		} 
		catch (IOException e)
		{
			System.out.print("IO error");
		}
	}
	
	public static void removePunctuation(ArrayList<String> list)
	{
		for(int i = 0; i < list.size(); i++)
		{
			list.set(i, list.get(i).replaceAll("[^a-zA-Z\\s]", ""));
		}
	}
	
	public static ArrayList<String> averageFile(File[] input)
	{
		ArrayList<String> list = new ArrayList<String>();
		
		for(int i = 0; i < input.length; i++)
		{
			fileToArrayList(input[i], list);
		}
		return list;
	}
	
	public static double avgChar(ArrayList<String> list)
	{
		int sum = 0;
		for(int i = 0; i < list.size(); i++)
		{
			sum += list.get(i).length();
		}
		
		
		return (Math.floor((sum / (double) list.size()) * 100) / 100);
	}
	
	public static double avgWordLength(ArrayList<String> list)
	{
		double numWords = 0;
		int letters = 0;
		
		for(int i = 0; i < list.size(); i++)
		{
			String[] words = list.get(i).split(" +");
			numWords += words.length;
			for(int j = 0; j < words.length; j++)
			{
				letters += words[j].length();
			}
		}
		
		return (Math.floor(((letters/numWords)) * 100) / 100);
	}
	
	public static int wordCount(ArrayList<String> list)
	{
		int numWords = 0;
		
		for(int i = 0; i < list.size(); i++)
		{
			String[] words = list.get(i).split(" +");
			for(int j = 0; j < words.length; j++)
			{
				if(!words[j].isEmpty())
				{
					numWords++;
				}
			}
			
		}
		return numWords;
	}
	
	public static int spacesCount(ArrayList<String> list)
	{
		int count = 0;
		for (int i = 0; i < list.size(); i++)
		{
			char[] line = list.get(i).toCharArray();
			for(int j = 0; j < line.length; j++)
			{
				if(line[j] == ' ')
				{
					count++;
				}
			}
		}
		
		return count;
	}
	
	public static int blankLineCount(ArrayList<String> list)
	{
		int count = 0;
		for(int i = 0; i < list.size(); i++)
		{
			if(list.get(i).isEmpty() || list.get(i) == null)
			{
				count++;
			}
		}
		
		return count;
	}
	
	public static String mostCommonWord(ArrayList<String> list) { //function returns most common word
		String result = ""; //variables
		String word = "";
		int count = 0;
		HashMap<String, Integer> wordMap = new HashMap<String, Integer>(); //create hashmap to store words and their count
		
		for(int i=0; i<list.size(); i++) { //go through list, store words as keys, store count as value
			String words[] = list.get(i).split(" +");
			for(int j = 0; j < words.length; j++)
			{
				word = words[j].toLowerCase();
				if(!word.isEmpty())
				{
					if(wordMap.containsKey(word)) //if the word is already a key, increase its count
						wordMap.put(word, wordMap.get(word) +1);
					else //else put the word in the map as a key, and set its count to 1
						wordMap.put(word, 1);		
				}
			}
		}
		word = "";
		
		Set<Map.Entry<String, Integer>> entrySet = wordMap.entrySet(); //entryset to traverse map
		for(Entry<String, Integer> entry : entrySet) { //go through map entries, find key with highest count
			if(entry.getValue()>count) { //compare key counts
				count = entry.getValue(); //record count if larger
				word = entry.getKey(); //record key with larger count
			} //else continue going through list
		}
		result = word + " " + "(" + Integer.toString(count) + ")"; //key (count)
	
		return result;
	}
	
}

